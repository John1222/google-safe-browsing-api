# Usage

```php
$api = new GoogleSafeBrowsingAPI( 'API_KEY' );
var_dump( $api->isURLBlacklisted( 'apk-get-update.info' ) ); // returns: true
var_dump( $api->isURLsBlacklisted( [
    'dom1.com',
    'dom2.com',
    'dom3.com',
    'apk-get-update.info'
] ) ); // returns: [ 'apk-get-update.info' ]
```