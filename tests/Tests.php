<?php
namespace Brocoder\GoogleSafeBrowsingAPI\Tests;

use Brocoder\GoogleSafeBrowsingAPI\GoogleSafeBrowsingAPI;
use Brocoder\GoogleSafeBrowsingAPI\GoogleSafeBrowsingAPIException;
use PHPUnit\Framework\TestCase;

class Tests extends TestCase
{
    private const APIKEY = 'API_KEY';
    private const URL_BLACKLISTED = 'apk-get-update.info';

    /**
     * After checking URLs with one detected must returns array with detected URL
     */
    public function testBatchOfURLsWithOneBlacklisted()
    {
        $gsb = new GoogleSafeBrowsingAPI( self::APIKEY );
        $result = $gsb->isURLsBlacklisted( [
            self::URL_BLACKLISTED,
            $this->generateCleanURL(), $this->generateCleanURL(), $this->generateCleanURL()
        ]);

        $this->assertCount( 1, $result );
        $this->assertEquals( self::URL_BLACKLISTED, $result[ 0 ] );
    }

    /**
     * After checking URLs with two detected must returns array with both detected URLs
     */
    public function testBatchOfURLsWithTwoBlacklisted()
    {
        $gsb = new GoogleSafeBrowsingAPI( self::APIKEY );
        $result = $gsb->isURLsBlacklisted([
            self::URL_BLACKLISTED,
            $this->generateCleanURL(),
            $this->generateCleanURL(),
            self::URL_BLACKLISTED,
            $this->generateCleanURL()
        ]);

        $this->assertCount( 2, $result );
        $this->assertEquals( self::URL_BLACKLISTED, $result[ 0 ] );
        $this->assertEquals( self::URL_BLACKLISTED, $result[ 1 ] );
    }

    public function testCheckSingleNotBlacklistedURL()
    {
        $gsb = new GoogleSafeBrowsingAPI( self::APIKEY );
        $this->assertFalse( $gsb->isURLBlacklisted( $this->generateCleanURL() ) );
    }

    public function testCheckSingleBlacklistedURL()
    {
        $gsb = new GoogleSafeBrowsingAPI( self::APIKEY );
        $this->assertTrue( $gsb->isURLBlacklisted( self::URL_BLACKLISTED ) );
    }
    
    public function testCheckWithBadAPI()
    {
        $gsb = new GoogleSafeBrowsingAPI( 'BAD_API' );
        $this->expectException( GoogleSafeBrowsingAPIException::class );
        $gsb->isURLBlacklisted( $this->generateCleanURL() );
    }
    
    private function generateCleanURL(): string
    {
        return 'a' . mt_rand( 1, 99999999 ) . '.com';
    }
}