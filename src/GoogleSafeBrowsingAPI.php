<?php
namespace Brocoder\GoogleSafeBrowsingAPI;

/**
 * Initial (default) limits
 *   Daily: 10k requests
 *   Speed: 100 reqeusts per 100 sec
 * 
 * In one request we can set <= 500 URLs.
 *
 * @see https://www.rsjoomla.com/support/documentation/rsfirewall-user-guide/frequently-asked-questions/how-to-generate-a-google-safe-browsing-api-key.html
 * @see https://developers.google.com/safe-browsing/v4/usage-limits
 */
class GoogleSafeBrowsingAPI
{
    /**
     * @var string
     */
    private $apikey;

    /**
     * @param string $apikey
     */
    public function __construct( string $apikey )
    {
        $this->apikey = $apikey;
    }

    /**
     * Returns array with detected URLs
     *
     * @param array $urls
     * @return array
     * @throws GoogleSafeBrowsingAPIException
     */
    public function isURLsBlacklisted( array $urls ): array
    {
        $dangerousUrls = [];
        $response = $this->sendRequest( $urls );
        if( empty( $response ) ) {
            return [];
        }
        if( isset( $response[ 'error' ] ) ) {
            throw new GoogleSafeBrowsingAPIException(
                "While checking urls occurred the error: " . $response[ 'error' ][ 'message' ]
            );
        }
        foreach( $response[ 'matches' ] as $match ) {
            $dangerousUrls[] = $match[ 'threat' ][ 'url' ];
        }
        return $dangerousUrls;
    }

    /**
     * @param string $url
     * @return bool
     * @throws GoogleSafeBrowsingAPIException
     */
    public function isURLBlacklisted( string $url ): bool
    {
        return ! empty( $this->isURLsBlacklisted( [ $url ] ) );
    }

    private function sendRequest( array $urls ): array
    {
        $urlsAsPartOfGeneralRequest = '';
        foreach( $urls as $url ) {
            $urlsAsPartOfGeneralRequest .= "{\"url\": \"$url\"},\r\n";
        }
        $urlsAsPartOfGeneralRequest = rtrim( $urlsAsPartOfGeneralRequest, ',' );

        $generalRequest = '{
      "client": {
        "clientId": "TestClient",
        "clientVersion": "1.0"
      },
      "threatInfo": {
        "threatTypes":      ["MALWARE", "SOCIAL_ENGINEERING"],
        "platformTypes":    ["LINUX"],
        "threatEntryTypes": ["URL"],
        "threatEntries": [
          ' . $urlsAsPartOfGeneralRequest . '
        ]
      }
      }';

        $ch = curl_init();
        curl_setopt(
            $ch, CURLOPT_URL, 'https://safebrowsing.googleapis.com/v4/threatMatches:find?key=' . $this->apikey
        );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER,
            array( "Content-Type: application/json", 'Content-Length: ' . strlen( $generalRequest ) )
        );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $generalRequest );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
        $responseAsArray = ( array ) json_decode( curl_exec( $ch ), true );
        curl_close( $ch );
        return $responseAsArray;
    }
}